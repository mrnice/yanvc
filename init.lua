require('plugins')
require('dap-config')
require('lsp')
require('config.keybinding')
-- FIMXE: these settings are taken and rewritten from lvim
--        do something similiar for the defaults
--        and keymappings
local settings = require('config.settings')
settings.load_defaults()
