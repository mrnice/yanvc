local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
  'mfussenegger/nvim-dap', -- Debug Adapter Plugin
  {
    'rcarriga/nvim-dap-ui', -- Debug Adapter Plugin UI
    dependencies = { 'mfussenegger/nvim-dap' }
  },
  'theHamsta/nvim-dap-virtual-text', -- Debug Adapter Plugin text
  'nvim-treesitter/nvim-treesitter', -- Treesitter
  'neovim/nvim-lspconfig', -- LSP configuration made easy
  'williamboman/nvim-lsp-installer', -- LSP installer 
  'p00f/clangd_extensions.nvim', -- Nice clangd extension to get information about inheritance
  'nvim-lua/lsp-status.nvim', -- LSP status line
  {
    'nvim-telescope/telescope.nvim', -- The well know and awesome telescope plugin
    tag = '0.1.0',
    dependencies = { {'nvim-lua/plenary.nvim'} }
  },
  {
    "navarasu/onedark.nvim", -- Onedark color scheme
    lazy = false,
    priority = 1000,
    config = function()
      require('onedark').setup {
        style = 'darker'
      }
      require('onedark').load()
    end
  },
  {
    'nvim-lualine/lualine.nvim',  -- Status line for neovim
    config = function ()
      require("lualine").setup()
    end
  },
  {
    'akinsho/bufferline.nvim', -- Buffer line for neovim inspired by emacs doom
    config = function ()
      require("bufferline").setup()
    end
  },
  'wsdjeg/vim-fetch', -- Yes nvim foo.bar:10:10 does exactly what I want it to do
  {
    'goolord/alpha-nvim', -- Nice startup page
    dependencies = { {'nvim-tree/nvim-web-devicons'} },
    config = function ()
      require('alpha').setup(require'alpha.themes.dashboard'.config)
    end
  },
  {
    'folke/which-key.nvim',
    lazy = false,
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      require("which-key").setup({
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      })
    end,
  },
  {
    'nvim-tree/nvim-tree.lua',
    version = "*",
    dependencies = {
      'nvim-tree/nvim-web-devicons',
    },
    config = function()
      require("nvim-tree").setup {}
    end,
  },
  'vala-lang/vala.vim',
  {
    'hrsh7th/cmp-nvim-lsp',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'hrsh7th/cmp-cmdline',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'hrsh7th/cmp-path',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'hrsh7th/cmp-buffer',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'hrsh7th/cmp-calc',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'f3fora/cmp-spell',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'tamago324/cmp-zsh',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'hrsh7th/cmp-nvim-lsp-signature-help',
    dependencies = { 'hrsh7th/nvim-cmp' }
  },
  {
    'L3MON4D3/LuaSnip',
    dependencies = { 'hrsh7th/nvim-cmp' },
  },
  {
    'hrsh7th/nvim-cmp',
    dependencies = { 'neovim/nvim-lspconfig' },
  },
  {
    'lewis6991/gitsigns.nvim',
    config = function ()
      require("gitsigns").setup()
    end
  },
  {
    "williamboman/mason.nvim",
    dependencies = { 'williamboman/mason-lspconfig.nvim', 'neovim/nvim-lspconfig'  },
    config = function ()
      require("mason.api.command").MasonUpdate()
      require("mason").setup()
    end
  },
  {
    'rcarriga/nvim-dap-ui',
    config = function ()
      require("dapui").setup()
    end
  },
  {
    "ellisonleao/glow.nvim",
    config = true,
    cmd = "Glow",
  },
  {
    "simrat39/symbols-outline.nvim",
    config = function ()
      require("symbols-outline").setup()
    end
  },
  {
    "sindrets/diffview.nvim",
  },
  {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },
  {
    'wfxr/minimap.vim',
    build = "cargo install --locked code-minimap",
    -- cmd = {"Minimap", "MinimapClose", "MinimapToggle", "MinimapRefresh", "MinimapUpdateHighlight"},
    config = function ()
      vim.cmd ("let g:minimap_width = 10")
      vim.cmd ("let g:minimap_auto_start = 1")
      vim.cmd ("let g:minimap_auto_start_win_enter = 1")
    end,
  },
  {
    "andymass/vim-matchup",
    config = function ()
      vim.g.matchup_matchparen_offscreen = { method = "popup" }
    end,
  },
  {
    "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    config = function()
      require("lsp_lines").setup()
    end,
  },
  {
    "lervag/vimtex",
  },
  {
    "andrewferrier/wrapping.nvim",
    config = function()
      require("wrapping").setup()
    end
  },
  {
    "https://gitlab.com/mrnice/ifdefcomment",
    config = function ()
      require("ifdefcomment").setup()
    end
  },
}

-- FIXME: clean up and move config to somewhere ...
vim.diagnostic.config({
  virtual_text = false,
})

require("lazy").setup(plugins, opts)

vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

require('luasnip.loaders.from_vscode').lazy_load()

local cmp = require('cmp')
local luasnip = require('luasnip')

local select_opts = {behavior = cmp.SelectBehavior.Select}

cmp.setup({
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end
  },
  sources = {
    {name = 'path'},
    {name = 'nvim_lsp', keyword_length = 1},
    {name = 'buffer', keyword_length = 3},
    {name = 'luasnip', keyword_length = 2},
  },
  window = {
    documentation = cmp.config.window.bordered()
  },
  formatting = {
    fields = {'menu', 'abbr', 'kind'},
    format = function(entry, item)
      local menu_icon = {
        nvim_lsp = 'λ',
        luasnip = '⋗',
        buffer = 'Ω',
        path = '🖫',
      }

      item.menu = menu_icon[entry.source.name]
      return item
    end,
  },
  mapping = {
    ['<Up>'] = cmp.mapping.select_prev_item(select_opts),
    ['<Down>'] = cmp.mapping.select_next_item(select_opts),

    ['<C-p>'] = cmp.mapping.select_prev_item(select_opts),
    ['<C-n>'] = cmp.mapping.select_next_item(select_opts),

    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),

    ['<C-e>'] = cmp.mapping.abort(),
    ['<C-y>'] = cmp.mapping.confirm({select = true}),
    ['<CR>'] = cmp.mapping.confirm({select = false}),

    ['<C-f>'] = cmp.mapping(function(fallback)
      if luasnip.jumpable(1) then
        luasnip.jump(1)
      else
        fallback()
      end
    end, {'i', 's'}),

    ['<C-b>'] = cmp.mapping(function(fallback)
      if luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {'i', 's'}),

    ['<Tab>'] = cmp.mapping(function(fallback)
      local col = vim.fn.col('.') - 1

      if cmp.visible() then
        cmp.select_next_item(select_opts)
      elseif col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        fallback()
      else
        cmp.complete()
      end
    end, {'i', 's'}),

    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item(select_opts)
      else
        fallback()
      end
    end, {'i', 's'}),
  },
})
