local dap = require('dap')
local mason_registry = require("mason-registry")
local cppdbg = mason_registry.get_package("cpptools")

dap.adapters.cppdbg = {
  id = 'cppdbg',
  type = 'executable',
  command = cppdbg:get_install_path() .. '/extension/debugAdapters/bin/OpenDebugAD7',
}
