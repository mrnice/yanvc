# Readme.md

Hey dear reader,
this is just my personal neovim approach and most of it is a copy of different preconfigured environments. Because I found it hard to customize them without knowing how the plugins are used and put together. So this is my playground to test and understand that plugins and plugin configurations.

If you want to have something useful and polished I recommend lunarvim, as a lot of my configuration is borrowed from there anyways.

# Used plugins

* https://github.com/mfussenegger/nvim-dap
* https://github.com/rcarriga/nvim-dap-ui
* https://github.com/navarasu/onedark.nvim
* https://github.com/goolord/alpha-nvim
* https://github.com/williamboman/nvim-lsp-installer
* https://github.com/neovim/nvim-lspconfig
* https://github.com/p00f/clangd_extensions.nvim
* TODO: complete list

# Plugins to consider

## found online
* https://vimawesome.com/
* https://vimcolorschemes.com/
* https://github.com/linty-org/readline.nvim
* https://github.com/brymer-meneses/grammar-guard.nvim
* https://github.com/iamcco/markdown-preview.nvim
* https://github.com/AckslD/nvim-neoclip.lua

## from lvim

* https://github.com/tamago324/nlsp-settings.nvim
* https://github.com/rcarriga/nvim-notify

# Random documentation

## lua documentation

* https://www.lua.org/pil/contents.html

## Open a file in new buffer of an existing nvim instance

Easiest solution is to install [neovim-remote](https://github.com/mhinz/neovim-remote)

Start a nvim process with

```bash
nvim --listen /tmp/nvimsocket
```

For nvim-qt use

```bash
nvim-qt -- --listen /tmp/nvimsocket
```

For nvim-qt with lunar vim

```bash
NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvim-qt --nvim lvim
```

Then use
```bash
nvr foo/bar.cpp
```

To open foo/bar.cpp in a new buffer

## Search for commands

```vim
q: q? q/
```

# TODO:
* test https://github.com/linty-org/readline.nvim

* Find out how to autocmd LspInstall on filetype
* Find out how to autocmd TSInstall on filetype


# LSP

```vim
LspInstall vala_ls
LspInstall clangd
```

# Treesitter

```vim
TSInstall cpp
TSInstall vala
```